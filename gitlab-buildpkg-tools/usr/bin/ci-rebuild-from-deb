#! /bin/bash

die() {
    echo "$(basename "$0"):" "$@" >&2 ; exit 1
}

# if not set, define RESULT_DIR from CI or use default
: "${RESULT_DIR:=${CI_PROJECT_DIR:-.}/result}"
declare -p RESULT_DIR

# get release information and compute RELEASE_DIR
RELEASE=$(ci-release --full)
RELEASE_ID=$(cut -d/ -f1 <<< "$RELEASE")
RELEASE_NAME=$(cut -d/ -f2 <<< "$RELEASE")
RELEASE_NUM=$(cut -d/ -f3 <<< "$RELEASE")
RELEASE_DIR="$RESULT_DIR/$RELEASE_ID/$RELEASE_NAME"
declare -p RELEASE RELEASE_ID RELEASE_NAME RELEASE_NUM RELEASE_DIR

# if not set, get options from CI or use defaults
: "${REBUILD_SECTION:=${PKG_REBUILD_SECTION:-non-free}}"
: "${REBUILD_DIST:=${PKG_REBUILD_DIST:-$RELEASE_NAME}}"
: "${REBUILD_WITH_SHLIBS:=${PKG_REBUILD_WITH_SHLIBS:-true}}"
: "${REBUILD_BUMP:=${PKG_REBUILD_BUMP:-$CI_PIPELINE_IID}}"
: "${REBUILD_EVERYWHERE:=${PKG_REBUILD_EVERYWHERE:-false}}"
declare -p REBUILD_DIST REBUILD_SECTION REBUILD_WITH_SHLIBS REBUILD_BUMP REBUILD_EVERYWHERE


update_in_dir() { # debfile
    local debfile="$1"
    echo "== Update in dir: $(pwd)"
    [ -f debian/control ] || die "not found: debian/control"
    [ -f "$debfile" ] || die "not found: $debfile"

    # get current control properties
    local section depends predepends conflicts pkgname
    section=$(dpkg-deb -f "$debfile" Section)
    depends=$(dpkg-deb -f "$debfile" Depends)
    predepends=$(dpkg-deb -f "$debfile" Pre-Depends)
    conflicts=$(dpkg-deb -f "$debfile" Conflicts)
    pkgname=$(dpkg-deb -f "$debfile" Package)

    # add original changelog to alien generated one
    [ -s debian/changelog ] || die "empty file: debian/changelog"
    local chname
    for chname in changelog.gz changelog.Debian.gz ; do
        if [ -e "usr/share/doc/$pkgname/$chname" ] ; then
            echo >> debian/changelog
            zcat "usr/share/doc/$pkgname/$chname" >> debian/changelog
            break
        fi
    done

    # compute new control properties
    case "$section" in
        '')     section="$REBUILD_SECTION" ;;
        */*)    section="$REBUILD_SECTION/${section##*/}" ;;
        *)      section="$REBUILD_SECTION/$section" ;;
    esac
    declare -p section depends predepends conflicts pkgname

    # update new control properties
    sed -i -e "s|Section:.*|Section: ${section,,}|" debian/control
    if [[ "${REBUILD_WITH_SHLIBS,,}" =~ ^(true|yes)$ ]] ; then
        # Alien always produces 'Depends: ${shlibs:Depends}' and ignores
        # declared dependencies in original packages.
	sed -i -e "s/\(^Depends:.*\)/\1,$depends/" debian/control
    else
        sed -i -e "s/^Depends:.*/Depends: $depends/" debian/control
        sed -i '/dh_shlibdeps/d' debian/rules
	# dh_compress ? d_makeshlibs ?
    fi
    if [ -n "$predepends" ] ; then
         # Alien add Pre-Depends value to Depends property, but it was
         # replaced with original Depends value above ...
         sed -i "/^Depends:/a Pre-depends: $predepends " debian/control
    fi
    if [ -n "$conflicts" ] ; then
        # Alien does not output Conflicts: so add it just after Depends:
        sed -i "/^Depends:/a Conflicts: $conflicts" debian/control
    fi
    if [[ "${REBUILD_EVERYWHERE,,}" =~ ^(true|yes)$ ]] ; then
         # special tag used by queue process trigger to copy package
         # on every allowed distribution configured in Allow: property
         # (see man deb-src-control).
         sed -i "/^Source:/a XC-Private-Everywhere: $REBUILD_EVERYWHERE" debian/control
    fi

    # update changelog
    dch -i --distribution "$REBUILD_DIST" \
        --check-dirname-level 0 --package "$pkgname" \
        "Rebuild to insert in section $section of distribution $REBUILD_DIST" 
}

build_in_dir(){
    echo "== Build in dir: $(pwd)"
    [ -f debian/control ] || die "not found: debian/control"
    debuild --build=binary -us -uc && debclean
}

move_package() { # pkgname
    local pkgname="$1"
    echo "== Move package: $pkgname"
    [ -d "$RELEASE_DIR" ] || mkdir -p "$RELEASE_DIR"
    mv -fv --target-directory="$RELEASE_DIR" \
        "$pkgname"_*.{deb,build,buildinfo,changes} \
        || die "mv failed"
}

rebuild_deb() { # debfile
    local debfile="$1" pkgname version major minor pkgdir bump
    echo "== Rebuild: $debfile"
    [ -f "$debfile" ] || die "not found: $debfile"

    # get package properties
    pkgname=$(dpkg-deb -f "$debfile" Package)
    version=$(dpkg-deb -f "$debfile" Version)
    major=$(echo "${version}-" | cut -d- -f1)
    minor=$(echo "${version}-" | cut -d- -f2)
    bump=${REBUILD_BUMP:-1}
    declare -p pkgname version major minor bump

    [ -n "$pkgname" ] || die "pkgname is empty"
    [ -n "$major" ] || die "major is empty"
    [ -d "$pkgname-$major" ] && rm -rf "$pkgname-$major"

    # extract original package
    export EMAIL="$DEBEMAIL"
    alien -gc --bump "$bump" "$1" || die "alien failed."

    # compute package dir and absolute deb file name
    if [ -d "$pkgname-$major" ] ; then
        pkgdir="$pkgname-$major"
    else
        pkgdir="$pkgname"
        #pkgdir=$(ls -d $pkgname* | grep -v .orig$)
    fi

    debfile="$(readlink -f "$debfile")" 
    declare -p pkgdir debfile
    [ -d "$pkgdir" ] || die "pkgdir not found: $pkgdir"

    (cd "$pkgdir" && update_in_dir "$debfile" ) || die "update debian failed."
    (cd "$pkgdir" && build_in_dir) || die "debuild failed."
    move_package "$pkgname"
}


if [ $# -eq 0 ] ; then
    echo "Usage $0 <existing file or url >.deb" >& 2
    exit 1
fi
[ -f /usr/bin/alien ] || die "alien not found"

# download remote files
pkglist=()
for arg in "$@" ; do
    case "$arg" in 
        http:*|https:*)
            mkdir -p download
            pkg=download/$(basename "$arg")
            curl "$arg" --output "$pkg" || die "curl failed."
            pkglist+=( "$pkg" )
            ;;
        *)
            echo "adding $arg"
            pkglist+=( "$arg" )
            ;;
     esac
done

# process files
for pkg in "${pkglist[@]}" ; do
    echo "Processing package: $pkg"
    case "$pkg" in
        *.deb)
            [ -f "$pkg" ] || die "file not found: $pkg"
            rebuild_deb "$pkg" || die "rebuild failed."
            ;;
        *)
            die "unknown file type: $pkg"
            ;;
    esac
done

