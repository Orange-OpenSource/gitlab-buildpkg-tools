# Move files in a temporary dir not to polute 
cp -r /tobuild /tmp/build
cd /tmp/build

# Upgrade OS
echo "===== Upgrade OS"
apt-get update > /tmp/01-apt-update.log
apt-get -y upgrade > /tmp/02-apt-upgrade.log

# Build package 
echo "===== Running ci-build-pkg"
ci-build-pkg > /tmp/03-ci-build-pkg.log

# Install just builded package
echo "===== Installing packages"
dpkg -i /tmp/build/result/debian/buster/gitlab*deb > /tmp/04-pkg-install.log 

cd /root
# Install and configure apache
echo "===== Install and configure apache"
apt-get -y install apache2 > /tmp/05-install-apache.log
a2ensite buildpkg-repo-staging-alias
a2ensite buildpkg-repo-production-alias
a2ensite buildpkg-repo-staging-vhost
a2ensite buildpkg-repo-production-vhost
a2dissite 000-default
sed -i -e 's/	ServerName.*/	ServerName production.example.com/' /etc/apache2/sites-available/buildpkg-repo-production-vhost.conf
sed -i -e 's/	ServerName.*/	ServerName staging.example.com/' /etc/apache2/sites-available/buildpkg-repo-staging-vhost.conf
echo "127.0.1.1	production.example.com" >> /etc/hosts
echo "127.0.1.1	staging.example.com" >> /etc/hosts
service apache2 start

# Install and configure ssh
echo "===== Install and configure ssh"
apt-get -y install ssh > /tmp/10-install-ssh.log
service ssh start
echo "===== Install and configure rsyslog"
apt-get -y install rsyslog > /tmp/11-install-rsyslog.log
service rsyslog start

## Simulate gitlab vars for ssh
echo "===== Simulate gitlab vars for ssh"
mkdir .tmpssh
ssh-keygen -b 2048 -t rsa -f .tmpssh/id_rsa -q -N ""
export SSH_PRIVATE_KEY=$(cat .tmpssh/id_rsa)

# Allow ssh access to current user (root) for production@$(hostname -f) and staging@$(hostname -)
echo "===== Install ssh keys"
install -d /var/lib/gitlab-buildpkg-repo/production/.ssh -o production -g production
install .tmpssh/id_rsa.pub /var/lib/gitlab-buildpkg-repo/production/.ssh/authorized_keys -o production -g production
install -d /var/lib/gitlab-buildpkg-repo/staging/.ssh -o staging -g staging
install .tmpssh/id_rsa.pub /var/lib/gitlab-buildpkg-repo/staging/.ssh/authorized_keys -o staging -g staging
# Register $(hostname -f) in known_hosts
[ -d ~/.ssh ] || mkdir ~/.ssh
ssh-keyscan -t rsa,dsa $(hostname -f) > ~/.ssh/known_hosts
# Check everything is ok
ssh -i .tmpssh/id_rsa staging@$(hostname -f) id -a
ssh -i .tmpssh/id_rsa production@$(hostname -f) id -a
#ssh -i .gitlab/ssh/id_rsa -o StrictHostKeyChecking=no staging@$(hostname -f) id -a
#ssh -i .gitlab/ssh/id_rsa -o StrictHostKeyChecking=no production@$(hostname -f) id -a

# Generate a GNUPG bot key pair that I will use to sign
echo "===== Generate gnupg"
export GNUPG_MAIL="bot@example.com"
cat > ~/.gnupgkeyfile <<-EOF
%no-protection
%echo Generating a standard key
Key-Type: RSA
Key-Length: 4096
Subkey-Type: ELG-E
Subkey-Length: 4096
Name-Real:  Bot key
Name-Comment: with stupid passphrase
Name-Email: $GNUPG_MAIL
Expire-Date: 0
%commit
%echo done
EOF
gpg --batch --gen-key ~/.gnupgkeyfile
# Trust the key
echo -e "5\ny\n" |  gpg --command-fd 0 --expert --edit-key $GNUPG_MAIL trust

## Simulate gitlab vars for gnupg
echo "===== Simulate gitlab vars for gnupg"
export GPG_PRIVATE_KEY=$(gpg --export-secret-keys --armor $GNUPG_MAIL)

# Push the key on the staging and production repo
echo "===== Push the key on the staging and production repo"
gpg --export-secret-keys --armor $GNUPG_MAIL | ssh -i .tmpssh/id_rsa staging@$(hostname -f)  gpg --import -
gpg --export-secret-keys --armor $GNUPG_MAIL | ssh -i .tmpssh/id_rsa production@$(hostname -f)  gpg --import -
# Declare as the key to use
sed -i -e "s/#STAGING_SIGNWITH=.*/STAGING_SIGNWITH=\"$GNUPG_MAIL\"/" /etc/default/gitlab-buildpkg-repo
sed -i -e "s/#PRODUCTION_SIGNWITH=.*/PRODUCTION_SIGNWITH=\"$GNUPG_MAIL\"/" /etc/default/gitlab-buildpkg-repo
echo ==========================================
grep SIGNWITH /etc/default/gitlab-buildpkg-repo
echo ==========================================

# Add components
sed -i -e "s/#DEBIAN_COMPONENTS=.*/DEBIAN_COMPONENTS=\"main contrib non-free\"/" /etc/default/gitlab-buildpkg-repo
sed -i -e "s/#UBUNTU_COMPONENTS=.*/UBUNTU_COMPONENTS=\"main restricted universe multiverse\"/" /etc/default/gitlab-buildpkg-repo

# Reconfigure package to take in account the new default
echo "===== Reconfigure gitlab-buildpkg-repo"
dpkg-reconfigure -plow gitlab-buildpkg-repo

cd /tmp/build
cp -r result/* /tmp/result
chown -R $result_owner /tmp/result
cd /tmp

echo "===== ci-sign-pkg"
gpg --list-secret-keys
ls -al result
ci-sign-pkg
echo "===== ci-deploy-pkg staging"
ci-deploy-pkg staging
echo "===== ci-deploy-pkg production"
ci-deploy-pkg production

chown -R $result_owner /tmp/result

export no_proxy="production.example.com,staging.example.com"
curl -s http://production.example.com/debian/ | grep pool
curl -s http://staging.example.com/debian/ | grep pool
